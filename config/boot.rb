ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.

# For whatever reason, this line below makes RGeo::Geos.supported? => false in TEST env
#require 'bootsnap/setup' # Speed up boot time by caching expensive operations.
