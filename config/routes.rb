Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :trip_paths, only: [] do
        collection do
          get 'events', to: 'trip_paths#entrance_and_exit_events'
          get 'search', to: 'trip_paths#find_by_trip_and_coordinates'
        end
      end
      resources :trips, only: [] do
        collection do
          post 'create_or_update', to: 'trips#create_or_update'
          get 'search', to: 'trips#search'
        end
      end
      resources :routes
      resources :buses, only: [:index] do
        collection do
          get 'search', to: 'buses#by_license_plate'
        end
      end
    end
  end
end
