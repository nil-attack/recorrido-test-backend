require 'rails_helper'

RSpec.describe TripPath, type: :model do
  describe "With different distances" do

    before :each do
      @trip = Trip.create!(route: Route.first, started_at: Time.now - 3.hours, bus: Bus.first)
      @trip_path_inside = TripPath.create(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)")
      @trip_within_100_meters = TripPath.create(trip: @trip, lonlat: "POINT(-70.71513175964355 -33.63776614677609)")
      @trip_outside = TripPath.create(trip: @trip, lonlat: "POINT(-70.71699857711792 -33.637658958389345)")
    end

    it "should have status == 'within route' when inside of its Trip's polygon" do
      expect(@trip_path_inside.point_on_route?).to be(true)
      expect(@trip_path_inside.status).to eq(TripPath::STATUSES[:within_route])
    end

    it "should have status == 'within route' when within 100 mts of its Trip's polygon" do
      expect(@trip_within_100_meters.point_on_route?).to be(true)
      expect(@trip_within_100_meters.status).to eq(TripPath::STATUSES[:within_route])
    end

    it "should have status == 'outside route' when outside the Trip's polygon and not within 100 mts of it" do
      expect(@trip_outside.point_on_route?).to be(false)
      expect(@trip_outside.status).to eq(TripPath::STATUSES[:outside_route])
    end
  end

  describe "Counts time spent at coordinates (with a margin)" do
    it "should count the number of seconds spent on a coordinate" do
      @trip = Trip.create!(route: Route.first, started_at: Time.now - 3.hours, bus: Bus.first)
      initial_time = Time.now
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.780770778656 -34.10213942555088)", created_at: initial_time)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.78169345855711 -34.10231710334376)", created_at: initial_time + 15.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.78115701675415 -34.10235263885757)", created_at: initial_time + 20.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.78130722045898 -34.10205058651454)", created_at: initial_time + 30.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.78083515167236 -34.10231710334376)", created_at: initial_time + 50.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 129.seconds)


      # Because it's one "group", in the end the time spent is "created_at" of the first report minus "created_at" of the
      # last report. Currently this will fail if the bus somehow goes back to being in range of those coordinates,
      # so we'll have to look at this over to make an SQL query that groups time spent per "group" and then sums that
      # TODO: Make this consider time per group and then add them (which is a big SQL query to write)
      expected_time_spent = 129
      res = TripPath.seconds_near_coordinates(@trip.id, -33.63768575549854, -70.71426272392273)
      expect(res[:seconds_spent]).to eq(expected_time_spent)
      expect(res[:started_at].utc.iso8601).to eq(initial_time.utc.iso8601)
    end

    it "should return nil if no time was spent on a coordinate" do
      @trip = Trip.create!(route: Route.first, started_at: Time.now - 3.hours, bus: Bus.first)
      initial_time = Time.now
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 15.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 20.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 30.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 50.seconds)
      TripPath.create!(trip: @trip, lonlat: "POINT(-70.71426272392273 -33.63768575549854)", created_at: initial_time + 129.seconds)
      res = TripPath.seconds_near_coordinates(@trip.id, -50, -120)
      expect(res.nil?).to be(true)
    end

    it "should return a list of points where the bus entered or left the route" do
      initial_time = Time.now
      @trip = Trip.create!(route: Route.first, started_at:  initial_time, bus: Bus.first)

      coords = [
          [
              -70.6913673877716, # within
              -33.45629325706671
          ],
          [
              -70.69211840629578, # within
              -33.4565796930371
          ],
          [
              -70.69286942481995, # out
              -33.45722417051078
          ],
          [
              -70.69188237190247, # within
              -33.45726892571302
          ],
          [
              -70.69087386131287, #within
              -33.4571704642376
          ],
          [
              -70.69290161132811, # out
              -33.458325141779774
          ]
      ]
      new_time = initial_time
      coords.each do |coord|
        TripPath.create!(trip: @trip, lonlat: "POINT(#{coord[0]} #{coord[1]})", created_at: new_time)
        new_time = new_time + 15.seconds
      end

      resp = TripPath.points_where_left_or_reentered_route(@trip.id)
      expect(resp.length).to eq(4)
      expect(resp.first.lonlat.to_s).to eq("POINT (-70.6913673877716 -33.45629325706671)")
      expect(resp.second.lonlat.to_s).to eq("POINT (-70.69286942481995 -33.45722417051078)")
      expect(resp.third.lonlat.to_s).to eq("POINT (-70.69188237190247 -33.45726892571302)")
      expect(resp.fourth.lonlat.to_s).to eq("POINT (-70.69290161132811 -33.458325141779774)")
    end

  end

end
