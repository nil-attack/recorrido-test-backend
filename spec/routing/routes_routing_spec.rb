require "rails_helper"

RSpec.describe Api::V1::RoutesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/v1/routes").to route_to("api/v1/routes#index")
    end
  end
end
