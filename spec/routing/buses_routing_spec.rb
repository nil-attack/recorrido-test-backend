require "rails_helper"

RSpec.describe Api::V1::BusesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/v1/buses").to route_to("api/v1/buses#index")
    end

    it "routes to #by_license_plate" do
      expect(:get => "/api/v1/buses/search?patente=AA-BB-CC").to route_to("api/v1/buses#by_license_plate", :patente => "AA-BB-CC")
    end
  end
end
