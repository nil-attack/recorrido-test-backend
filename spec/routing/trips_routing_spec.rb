require "rails_helper"

RSpec.describe Api::V1::TripsController, type: :routing do
  describe "routing" do
    it "routes to #create_or_update" do
      expect(:post => "/api/v1/trips/create_or_update").to route_to("api/v1/trips#create_or_update")
    end
  end
end
