require 'net/http'
require 'json'

desc 'Bus on route tasks'
namespace :bus_on_route do
  # TODO: Finish this
  task :start do
    route = JSON.parse(File.read("#{Rails.root}/example_trips/follows_route_santiago_peor_es_nada.json"))
    coords = route["geometry"]["coordinates"]
    serial_gps = "7c75a83bfc10fe34199a33a409335f10" # Taken from a Bus in seeds
    url = 'http://0.0.0.0:3000/api/v1/create_or_update'
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)


    ## Currently this fails, not sure why it can't connect, maybe something about docker?
    (0..).lazy.each do |idx|
      lon, lat = coords[idx]
      params = {"latitude" => lat, "longitude" => lon, "device_serial_number" => serial_gps}
      request = Net::HTTP::Post.new(uri.path, 'Content-Type'  => 'application/json')

      request.body = params.as_json.to_json
      http.request(request)

      sleep 10
    end
  end

end
