class AddIndexOnTripPathsLonlat < ActiveRecord::Migration[6.0]
  def change
    add_index :trip_paths, :lonlat, using: :gist
  end
end
