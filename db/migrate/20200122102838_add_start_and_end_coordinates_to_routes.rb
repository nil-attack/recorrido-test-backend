class AddStartAndEndCoordinatesToRoutes < ActiveRecord::Migration[6.0]
  def change
    add_column :routes, :start_latitude, :decimal, null: false
    add_column :routes, :start_longitude, :decimal, null: false
    add_column :routes, :end_latitude, :decimal, null: false
    add_column :routes, :end_longitude, :decimal, null: false
  end
end
