class CreateTripPath < ActiveRecord::Migration[6.0]
  def change
    create_table :trip_paths do |t|
      t.references :trip, foreign_key: true, index: true
      t.st_point :lonlat, geographic: true
    end
  end
end
