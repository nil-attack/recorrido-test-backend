class CreateTrips < ActiveRecord::Migration[6.0]
  def change
    create_table :trips do |t|
      t.datetime :started_at
      t.datetime :finished_at
      t.references :bus, foreign_key: true, index: true
      t.references :route, foreign_key: true, index: true
      t.timestamps
    end
  end
end
