class AddTimestampsToTripPath < ActiveRecord::Migration[6.0]
  def change
    change_table :trip_paths do |t|
      t.timestamps
    end
  end
end
