class AddStatusToTripPaths < ActiveRecord::Migration[6.0]
  def change
    add_column :trip_paths, :status, :string
  end
end
