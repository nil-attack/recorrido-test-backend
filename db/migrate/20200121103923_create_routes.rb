class CreateRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :routes do |t|
      t.string :name
      t.geometry :polygon
      t.timestamps
    end
  end
end
