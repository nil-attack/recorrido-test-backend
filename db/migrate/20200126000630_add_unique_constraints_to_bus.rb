class AddUniqueConstraintsToBus < ActiveRecord::Migration[6.0]
  def change
    add_index :buses, :serial_gps, unique: true
    add_index :buses, :patente, unique: true
  end
end
