class CreateBuses < ActiveRecord::Migration[6.0]
  def change
    create_table :buses do |t|
      t.string :patente
      t.string :serial_gps
      t.timestamps
    end
  end
end
