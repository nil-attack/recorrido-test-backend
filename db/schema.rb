# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_26_185411) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "buses", force: :cascade do |t|
    t.string "patente"
    t.string "serial_gps"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["patente"], name: "index_buses_on_patente", unique: true
    t.index ["serial_gps"], name: "index_buses_on_serial_gps", unique: true
  end

  create_table "routes", force: :cascade do |t|
    t.string "name"
    t.geometry "polygon", limit: {:srid=>0, :type=>"geometry"}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "start_latitude", null: false
    t.decimal "start_longitude", null: false
    t.decimal "end_latitude", null: false
    t.decimal "end_longitude", null: false
  end

  create_table "trip_paths", force: :cascade do |t|
    t.bigint "trip_id"
    t.geography "lonlat", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.index ["lonlat"], name: "index_trip_paths_on_lonlat", using: :gist
    t.index ["trip_id"], name: "index_trip_paths_on_trip_id"
  end

  create_table "trips", force: :cascade do |t|
    t.datetime "started_at"
    t.datetime "finished_at"
    t.bigint "bus_id"
    t.bigint "route_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.index ["bus_id"], name: "index_trips_on_bus_id"
    t.index ["route_id"], name: "index_trips_on_route_id"
  end

  add_foreign_key "trip_paths", "trips"
  add_foreign_key "trips", "buses"
  add_foreign_key "trips", "routes"
end
