# README

### Requisitos:
Para ejecutar el proyecto se necesita `docker` y `docker-compose`

### Instalacion
Construimos el docker
`docker-compose build`
Instalamos dependencias
`docker-compose run --rm web bundle install` 

Creamos db, ejecutamos migraciones y añadimos datos de seeds
`docker-compose run --rm web db:create`
`docker-compose run --rm web db:migrate`
`docker-compose run --rm web db:seed`

Ejecutamos el app (se ejecuta en 0.0.0.0:80)
`docker-compose run --rm --service-ports web`

Puedes ejecutar los test con 
`docker-compose run --rm rspec`

### Información util
- Existen viajes creados los dias 10/enero/2020, 12/enero/2020 y 13/enero/2020
- Las rutas tomadas por los viajes pueden verse en "#{Rails.root}/example_trips"
- Las seeds crean cada uno de esos viajes de ida ("Santiago => Peor es Nada") y de vuelta (
llamando #reverse sobre las coordinates del json y asignandole la ruta de vuelta ("Peor es Nada => Santiago")
) (en total hacen 8)
- Los viajes (id=5, 12/01/2020) de Santiago a Peor es Nada) y (id=6, 12/01/2020 de Peor es Nada a Santiago)
- El ultimo viaje (id=9, 13/01/2019 de Santiago a Peor es Nada),creado a mano, muestra un viaje que inicia en el punto de inicio de su ruta y se mantiene siempre dentro de 100 mts (la idea es que sirva para contar los seg cerca de un punto)
(Est Central) y se mantiene durante 129 segundos (y luego no tiene mas puntos). Se pueden usar las coordenadas iniciales del viaje para
verificar esto, las cuales aparecen en el mismo modal que da la opcion de ingresar coordenadas

