class Trip < ApplicationRecord

  belongs_to :route
  belongs_to :bus
  has_many :trip_paths

  STATUSES = {
      incomplete: 'incomplete',
      completed: 'completed',
      on_route: 'on route'
  }.freeze

  SECONDS_THRESHOLD_BEFORE_TRIP_ENDS = 15 * 60

  def self.completed_trips
    where(status: STATUSES[:completed])
  end

  def self.incomplete_trips
    where(status: STATUSES[:incomplete])
  end

  def self.on_route_trips
    where(status: STATUSES[:on_route])
  end

  def self.latest_trip_by_bus(bus)
    Trip.where(bus: bus)
        .order("created_at DESC")
        .limit(1)
        .first
  end

  def self.latest_trip_by_gps_device(device_id)
    bus_with_the_device = Bus.find_by(serial_gps: device_id)

    Trip.where(bus: bus_with_the_device)
        .order("created_at DESC")
        .limit(1)
  end

  def self.search(route_id, date, license_plate = nil)
    day = DateTime.parse(date)
    if license_plate.nil?
      res = select("trips.id AS id, buses.patente, trips.started_at, trips.finished_at, trips.status")
                .joins(:bus)
                .where(route_id: route_id)
                .where("started_at BETWEEN ? AND ?", day.midnight, (day.tomorrow.midnight - 1.second))
      return res.as_json
    end
    bus = Bus.find_by(patente: license_plate)

    Trip.joins(:bus).select("trips.id AS id, buses.patente AS patente, trips.started_at AS started_at, trips.finished_at AS finished_at, trips.status AS status")
              .where(route_id: route_id)
              .where(buses: {patente: bus.patente})
              .where("started_at BETWEEN ? AND ?", day.midnight, (day.tomorrow.midnight - 1.second))
              .as_json
  end

  def on_route?
    status == STATUSES[:on_route]
  end

  def last_position_update_within_time_threshold?(current_time)
    last_position_update = self.trip_paths .last
    return false if last_position_update.nil?

    elapsed_seconds = (current_time - last_position_update.created_at).to_i
    elapsed_seconds < SECONDS_THRESHOLD_BEFORE_TRIP_ENDS
  end

  def near_end_of_route?
    current_route = self.route
    position = self.trip_paths.order("created_at DESC").limit(1).first
    lat, long = position.lonlat.lat, position.lonlat.lon

    route = Route.near_ending_point(lat, long).first
    current_route == route
  end

  def finish_incomplete!
    update_attributes(status: STATUSES[:incomplete], finished_at: Time.now)
  end

  def finish_complete!
    update_attributes(status: STATUSES[:completed], finished_at: Time.now)
  end

  def update_position(latitude, longitude)
    TripPath.create(trip: self, lonlat: "POINT (#{longitude} #{latitude})")
  end

end
