class Route < ApplicationRecord

  has_many :trips

  # This is meters, since using ST_Distance and casting its parameters
  # to 'geography' returns results in meters
  MAX_DISTANCE_MARGIN = 100

  # Returns the closest route where lat, long is within 100 meters of the route's starting point
  # This will need to be changed when more routes are added, because if you are in Estacion Central there are
  # going to be many routes that go Estacion Central -> Somewhere else. But for now it works
  def self.near_starting_point(latitude, longitude)
    sql = <<~SQL
      SELECT *
      FROM (
        SELECT ST_DISTANCE(
          ST_SetSRID(ST_MakePoint(routes.start_longitude, routes.start_latitude)::geography, 4326),
          ST_SetSRID(ST_MakePoint(?, ?)::geography, 4326)
         ) AS distance,
         routes.*
         FROM routes
      ) route_with_distance
      WHERE route_with_distance.distance <= ?
      ORDER BY route_with_distance.distance ASC
      LIMIT 1
    SQL
    find_by_sql(sanitize_sql_array([sql, longitude, latitude, Route::MAX_DISTANCE_MARGIN]))
  end

  def self.near_ending_point(latitude, longitude)
    sql = <<~SQL
      SELECT *
      FROM (
        SELECT ST_DISTANCE(
          ST_SetSRID(ST_MakePoint(routes.end_longitude, routes.end_latitude)::geography, 4326),
          ST_SetSRID(ST_MakePoint(?, ?)::geography, 4326)
         ) AS distance,
         routes.*
         FROM routes
      ) route_with_distance
      WHERE route_with_distance.distance <= ?
      ORDER BY route_with_distance.distance ASC
      LIMIT 1
    SQL
    find_by_sql(sanitize_sql_array([sql, longitude, latitude, Route::MAX_DISTANCE_MARGIN]))
  end
end
