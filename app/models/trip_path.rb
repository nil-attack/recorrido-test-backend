class TripPath < ApplicationRecord
  belongs_to :trip

  before_save do
    if self.point_on_route?
      self.status = TripPath::STATUSES[:within_route]
    else
      self.status = TripPath::STATUSES[:outside_route]
    end
  end

  SECONDS_THRESHOLD_STOPPED_AT_POINT = (2 * 60) # 2 minutes

  STATUSES = {
      within_route: 'within route',
      outside_route: 'outside route'
  }.freeze

  def self.seconds_near_coordinates(trip_id, lat, lon)
    sql = <<~SQL
      SELECT *
      FROM (
       SELECT ST_GeomFromText('POINT(? ?)')::geography AS point,
          ST_GeomFromText(?)::geography AS area_polygon, trip_paths.*
       FROM trip_paths
       WHERE trip_id = ?
      ) AS calc
      WHERE ST_DWithin(calc.point, calc.area_polygon, ?) = true
      ORDER BY calc.created_at ASC
    SQL

    area = Trip.find(trip_id).route.polygon.to_s
    sanitized = TripPath.sanitize_sql_array([sql, lon, lat, area,trip_id, Route::MAX_DISTANCE_MARGIN])

    res = TripPath.find_by_sql(sanitized)
    # This will be correct unless the bus somehow goes back to the
    # coordinates after leaving them (so that not all gps reports of the
    # bus are together. However, given the time constraints and the difficulty
    # of making an SQL query to aggregate the time spent per "group", I decided to leave
    # this "naive" solution for now, which still should be useful)
    # TODO: Refactor into SQL for more accuracy
    # TODO: Afterwards I noticed this doesn't correctly exclude points not within the MAX_DISTANCE_MARGIN
    #   so this will have to be refactored (currently it does not work correctly)

    if res.empty?
      return nil
    end
    {
        started_at: res.first.created_at,
        seconds_spent: (res.last.created_at - res.first.created_at).to_i
    }
  end

  def self.points_where_left_or_reentered_route(trip_id)
    # This can be done with a SQL query, but given the time constraints
    # it'll be left with SQL/Ruby.
    # TODO: Refactor this into pure SQL for performance

    trip_paths = TripPath.where(trip_id: trip_id)
                         .order("created_at ASC")
    events_of_note = []
    current_status = nil
    trip_paths.each do |tp|
      if tp.status != current_status
        events_of_note << tp
        current_status = tp.status
      end
    end
    events_of_note
  end


  def point_on_route?
    area = Trip.find(self.trip.id).route.polygon.to_s
    lat, lon = self.lonlat.lat, self.lonlat.lon
    sql = <<~SQL
      SELECT ST_DWithin(point, area_polygon, ?) As result
      FROM
        (
        SELECT ST_GeomFromText('POINT(? ?)')::geography AS point,
          ST_GeomFromText(?)::geography AS area_polygon) AS calc;
    SQL

    sanitized_sql = TripPath.sanitize_sql_array([sql, Route::MAX_DISTANCE_MARGIN,
                                                 lon, lat, area])

    query = ActiveRecord::Base.connection.execute(sanitized_sql)
    query.first["result"]
  end

end
