class Api::V1::TripsController < ApplicationController

  def search
    route_id = params[:route_id].to_i
    # Need to receive this date in UTC time/convert it to UTC
    # An easy (but not that friendly) way of guaranteeing that this will be
    # UTC is making the user select it (because if it is "autoselected" for the user
    # upon arriving at the view, it probably will select based on the user's timezone)
    date = params[:date]
    plate_number = params[:license_plate]
    license_plate =
        if plate_number == 'null' || plate_number == '' || plate_number.nil?
          nil
        else
          plate_number.to_s
        end

    if route_id.zero? || date.nil?
      message = missing_search_params_response(params)
      return render json: { message: message }, status: 422
    end

    trips = Trip.search(route_id, date, license_plate)
    render json: { trips: trips }
  end

  def create_or_update
    # Example data to be received
    #{
    #    "device_serial_number": "d131dd02c5e6eec4",
    #    "latitude": -70.69075584411621,
    #    "longitude": -33.45382270754665
    #}
    latitude = params[:latitude]
    longitude = params[:longitude]

    device_id = params[:device_serial_number]
    bus_with_the_device = Bus.find_by(serial_gps: device_id)

    # If no bus has that GPS device associated with it, return Not found
    if bus_with_the_device.blank?
      return render json: { message: 'No buses found with that GPS device.' }, status: 404
    end

    latest_trip = Trip.latest_trip_by_bus(bus_with_the_device)

    # If no trips have been made with the bus that currently has that device
    if latest_trip.nil?
      new_trip = start_new_trip_if_near_starting_position(bus_with_the_device, latitude, longitude)
      unless new_trip
        return render json: { message: 'Not near a starting position' }, status: 403
      end
      return render json: { trip: new_trip }
    end

    # If the trip is on route AND there has been no update within the time threshold
    # (which for this test project is 15 min) then we have to finish that trip with the
    # 'incomplete' status.
    # This is because a trip that's completed normally will have
    # its status set to 'complete' when the trip is 'on route' and the gps reports a position within
    # 100 meters of the position of the ending coordinates of the Route
    if latest_trip.on_route?
      is_updated_within_threshold = latest_trip.last_position_update_within_time_threshold?(Time.now)

      if is_updated_within_threshold
        latest_trip.update_position(latitude, longitude)
        latest_trip.finish_complete! if latest_trip.near_end_of_route?
        return render json: { trip: latest_trip }
      else
        # Finish the last trip as 'incomplete', since it did not report within the allowed time threshold
        # We should probably have a CRON or something similar checking for incomplete trips, this has been
        # made this way for demostrative purposes only
        latest_trip.finish_incomplete!
        # And create a new trip if the position is within 100 meters of a starting point
        new_trip = start_new_trip_if_near_starting_position(bus_with_the_device, latitude, longitude)
        unless new_trip
          return render json: { message: 'Not near a starting position' }, status: 403
        end
        return render json: { trip: new_trip }
      end
    end

    # If the latest trip exists and is not on route (which means it's either "complete" -> ended successfully
    # or "incomplete" -> ended unsuccessfully) and lat,long are near a route starting point,
    # create a new trip
    new_trip = start_new_trip_if_near_starting_position(bus_with_the_device, latitude, longitude)
    unless new_trip
      return render json: { message: 'Not near a starting position' }, status: 403
    end
    render json: { trip: new_trip }
  end

  def start_new_trip_if_near_starting_position(bus, latitude, longitude)
    nearest_route = Route.near_starting_point(latitude, longitude)

    return nil unless nearest_route.present?
    new_trip = Trip.create(started_at: Time.now,
                           bus: bus,
                           route: nearest_route.first,
                           status: Trip::STATUSES[:on_route])

    TripPath.create(trip: new_trip, lonlat: "POINT (#{longitude} #{latitude})")
    new_trip
  end

  def missing_search_params_response(params)
    date = params[:date]
    route_id = params[:route_id]
    message = ["Missing required parameters: "]
    missing_params = []
    missing_params << "date (DD-MM-YYYY)" if date.nil?
    missing_params << "route_id" if route_id.nil?
    missing_params = missing_params.join(", ")
    (message << missing_params).join
  end
end
