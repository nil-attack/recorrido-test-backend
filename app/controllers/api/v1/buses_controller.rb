class Api::V1::BusesController < ApplicationController

  def index
    render json: Bus.all
  end

  def by_license_plate
    plate = params[:patente]
    bus = Bus.find_by(patente: plate)
    render json: { bus: bus }
  end
end
