class Api::V1::TripPathsController < ApplicationController

  def find_by_trip_and_coordinates
    trip_id = params[:trip_id].to_i
    lat = params[:latitude].to_f
    lon = params[:longitude].to_f
    res = TripPath.seconds_near_coordinates(trip_id, lat, lon)
    render json: { result: res }
  end

  def entrance_and_exit_events
    trip_id = params[:trip_id]
    render json: {points: TripPath.points_where_left_or_reentered_route(trip_id) }
  end
end
