SHELL := /bin/bash

all: build db run

run: build
	docker-compose up -d sidekiq
	docker-compose run --service-ports web
	docker-compose stop sidekiq

build: .built .bundled

.built: Dockerfile.development
	docker-compose build
	touch .built

.bundled: Gemfile Gemfile.lock
	docker-compose run web bundle
	touch .bundled

stop:
	docker-compose stop

restart: build
	docker-compose restart web

clean: stop
	rm -f tmp/pids/*
	docker-compose rm -f -v bundle_cache
	rm -f .bundled
	docker-compose rm -f
	rm -f .built

test: build db
	docker-compose run web rake db:test:prepare
	docker-compose run web rails spec

logs:
	docker-compose logs

db: build
	docker-compose run web rake db:create db:migrate db:seed
